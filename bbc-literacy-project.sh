#!/bin/bash

HTTP_200_OK="200"

if [ ${#} -ne 4 ] ; then
    echo "Usage: <url> <series-number> <first-episode-number-in-sequence> <num-episodes>"
    echo "Example: ${0} 'https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/a9b57dbf8ac4145c6b0685170efb5478' 2 11 11"
    exit 65
fi

url="${1}"
series="${2}"
first_episode=$(("${3}" - 1))
num_episodes="${4}"

prefix="https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk"

series_title=$(curl -s "${url}" \
          | grep "<title>" \
          | cut -f2 -d">" \
          | cut -f1 -d"<"
       )

sanitised_title=$(echo "${series_title}" | sed -e "s/[\'\?\,]//g")

episode_asset_list=($(curl -s "${url}" \
                       | grep "Watch programme" \
                       | cut -f2 -d"/" \
                       | cut -f1 -d"\"" \
                    ))

episode_number=1

# Starting doing the work
echo "Fetching series ${series}: \"${series_title}\""

for asset in "${episode_asset_list[@]:${first_episode}:${num_episodes}}" ; do
    episode_title=$(curl -s "${url}" \
                      | grep -A1 ${asset} \
                      | grep 'img alt' \
                      | cut -f2 -d'"' \
                      | cut -c3- \
                      | sed -e "s/[\'\?\,]//g")
    echo "Fetching episode ${episode_number} of ${num_episodes}: \"${episode_title}\""
    for segment in $(seq 0 1000) ; do
        response=$(curl -s -w "%{http_code}" \
            "${prefix}/asset/video/${asset}/768x432/seg$(printf %08d ${segment}).ts" \
            -H 'User-Agent: Mozilla/5.0 (GEM; PANOS; BBC Master Scientific; rv:65.0) Gecko/20100101 Firefox/65.0' \
            -H 'Accept: */*' \
            -H 'Accept-Language: en-IE,en-GB;q=0.7,en;q=0.3' \
            --compressed \
            -H "Referer: ${prefix}/${asset}" \
            -H 'DNT: 1' \
            -H 'Connection: keep-alive' \
            -H 'Pragma: no-cache' \
            -H 'Cache-Control: no-cache' \
            -H 'TE: Trailers' \
            -O)
        if [ ${response} -ne ${HTTP_200_OK} ] ; then
            rm seg"$(printf %08d ${segment})".ts
            echo -e "\nDone."
            break
        fi
        printf "."
        if [ $(((${segment} + 1) % 20)) == 0 ] ; then
            printf '\n'
        fi
    done
    # Concatenate all the fragments into the final file and tidy up
    cat *.ts >"${sanitised_title} - s$(printf %02d ${series})e$(printf %02d ${episode_number}) - ${episode_title}.mp4"
    rm *.ts
    ((episode_number++))
done
